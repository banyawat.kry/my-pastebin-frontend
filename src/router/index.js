import Vue from "vue";
import VueRouter from "vue-router";
const Home = () => import("@/views/Home");
const About = () => import("@/views/About");
const View = () => import("@/views/View");
Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "home",
    component: Home
  },
  {
    path: "/view",
    name: "view",
    component: View,
    props: { default: true }
  },
  {
    path: "/about",
    name: "about",
    component: About
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

export default router;
