import axios from "axios";

class PasteDataService {
  retrieveAllPaste() {
    return axios.get(`/api/recents`);
  }
  retrievePaste(id) {
    return axios.get(`/api/${id}`);
  }

  createPaste(paste) {
    return axios.post("/api/paste", paste);
  }
}

export default new PasteDataService();
